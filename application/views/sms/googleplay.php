<?php $this->load->view('incl/main/head'); ?>
<div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <?php $this->load->view('incl/main/navbar'); ?>

    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
        <?php $this->load->view('incl/main/sidebar'); ?>

        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Google SMS</h4>
                                <div class="table-responsive">
                                    <table class="table" id="myTable">
                                        <thead>
                                            <tr>
                                                <th>msisdn</th>
                                                <th>cp</th>
                                                <th>process_time </th>
                                                <th>sending_time</th>
                                                <th>status</th>
                                                <th>channel</th>
                                                <th>number</th>
                                                <th>status_message</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                          for ($i=0; $i < json_decode($sms)->count; $i++) { 
                              $row = json_decode($sms)->response[$i];
?>
                                           <tr>
                                               <td><?= $row->msisdn; ?></td>
                                               <td><?= $row->cp; ?></td>
                                               <td><?= $row->process_time; ?></td>
                                               <td><?= $row->sending_time; ?></td>
                                               <td><?= $row->status; ?></td>
                                               <td><?= $row->channel;?></td>
                                               <td><?= $row->number; ?></td>
                                               <td><?= $row->status_message;?></td>

                                           </tr>
                          <?php } ;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- content-wrapper ends -->
            <!-- partial:partials/_footer.html -->
            <?php $this->load->view('incl/main/footer'); ?>

            <!-- partial -->
        </div>
        <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->

<!-- plugins:js -->
<?php $this->load->view('incl/main/script'); ?>
<script type="text/javascript">
  $(document).ready( function () {
    $('#myTable').DataTable();
} );
</script>
<!-- End custom js for this page-->
</body>

</html>