<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sms extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin:*');
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Max-Age: 1000');
        header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

        $this->load->model('SmsModel', 'sm');
    }

    public function googleplay()
    {
        $data['title'] = '';
        $data['namaMenu'] = '';
        $data['aktif'] = '';

        $data['sms'] = $this->sm->googleplay();

        $this->load->view('sms/googleplay', $data, false);
    }


}